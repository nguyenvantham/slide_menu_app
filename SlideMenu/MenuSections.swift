//
//  MenuItem.swift
//  SlideMenu
//
//  Created by Nguyễn Thắm on 1/22/19.
//  Copyright © 2019 Touch Space. All rights reserved.
//

import UIKit

enum MenuSection {
    case profile
    case menuItem
    
    static let allForClient: [MenuSection] = [
        .profile,
        .menuItem
    ]
    var title: String? {
        switch self {
        case .profile:
            return "Thông tin cá nhân"
        case .menuItem:
            return "Cac chuc nang"
        }
    }
    
}
enum MenuProfile {
    case profile
    case meaningfulLife
    static let allForClient: [MenuProfile] = [
        .profile,
        .meaningfulLife
    ]
    var title: String? {
        switch self {
        case .profile:
            return "Avatar, so du tai khoan, lich hen, ngay thang balabala"
        case .meaningfulLife:
            return "Move your feet or lose some toes"
        }
        
    }
}


enum MenuItem {
    case infoProfile
    case historyBooking
    case currentAppointment
    case payment
    case transaction
    case wallet
    case logout
    
    static let allForClient: [MenuItem] = [
        .infoProfile,
        .historyBooking,
        .transaction,
        .wallet,
        .logout
    ]
    
    var title: String? {
        switch self {
        case .infoProfile:
            return "Thông tin cá nhân"
        case .historyBooking:
            return "Lịch sử dịch vụ"
        case .currentAppointment:
            return "Lịch hẹn hiện tại"
        case .payment:
            return "Thanh toán"
        case .transaction:
            return "Lịch sử giao dịch"
        case .wallet:
            return "Ví điện tử"
        case .logout:
            return "Đăng xuất"
        }
    }
}
