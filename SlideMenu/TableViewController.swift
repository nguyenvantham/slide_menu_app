//
//  TableViewController.swift
//  SlideMenu
//
//  Created by Nguyễn Thắm on 1/22/19.
//  Copyright © 2019 Touch Space. All rights reserved.
//

import UIKit


class TableViewController: UITableViewController {
    
    var sections: [MenuSection]  {
        return MenuSection.allForClient
    }
    var menuItems: [MenuItem]  {
        return MenuItem.allForClient
        }
    var profile: [MenuProfile] {
        return MenuProfile.allForClient
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "my")
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch sections[section] {
        case .profile:
            return profile.count
        case .menuItem:
            return menuItems.count
        }
    
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section].title
    }
  
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "my")!
        
        switch sections[indexPath.section] {
        case .profile:
            cell.textLabel?.text   = profile[indexPath.row].title
            return cell
        
        case .menuItem:
            cell.textLabel?.text = menuItems[indexPath.row].title
            return cell
        }
    }

}
